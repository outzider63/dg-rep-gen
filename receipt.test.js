const generateReciept = require('./receipt');

test('Should calculate tax when several items are tax exempt', () => {
  const orderLines = [
      { 'qty' : 2,
        'item' : 'book',
        'price' : 12.49,
        'exempt' : true,
        'imported' : false
      },
      {
        'qty' : 1,
        'item' : 'music CD',
        'price' : 14.99,
        'exempt' : false,
        'imported' : false
      },
      {
        'qty' : 1,
        'item' : 'chocolate bar',
        'price' : 0.85,
        'exempt' : true,
        'imported' : false
      }
    ];

  const receipt = generateReciept(orderLines);
  expect(receipt.salesTax).toBe(1.50);
  expect(receipt.total).toBe(42.32);
});

test('Should calculate additional tax when import items are present', () => {
  const orderLines = [
      { 'qty' : 1,
        'item' : 'imported box of chocolates',
        'price' : 10.00,
        'exempt' : true,
        'imported' : true
      },
      {
        'qty' : 1,
        'item' : 'imported bottle of perfume',
        'price' : 47.50,
        'exempt' : false,
        'imported' : true
      }
    ];

  const receipt = generateReciept(orderLines);
  // expect(receipt.salesTax).toBe(7.65);
  expect(receipt.salesTax).toBe(7.63);
  // expect(receipt.total).toBe(65.15);
  expect(receipt.total).toBe(65.13);
});

test('Should calculate additional tax when order has mixed import and non-import items', () => {
  const orderLines = [
      { 'qty' : 1,
        'item' : 'imported box of perfume',
        'price' : 27.99,
        'exempt' : false,
        'imported' : true
      },
      {
        'qty' : 1,
        'item' : 'perfume',
        'price' : 18.99,
        'exempt' : false,
        'imported' : false
      },
      {
        'qty' : 1,
        'item' : 'packet of headache pills',
        'price' : 9.75,
        'exempt' : true,
        'imported' : false
      },
      {
        'qty' : 3,
        'item' : 'box of imported chocolates',
        'price' : 11.25,
        'exempt' : true,
        'imported' : true
      }
    ];

  const receipt = generateReciept(orderLines);
  // expect(receipt.salesTax).toBe(7.90);
  expect(receipt.salesTax).toBe(7.79);
  // expect(receipt.total).toBe(98.38);
  expect(receipt.total).toBe(98.27);
});