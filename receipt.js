function generateReceipt(orderLines) {

  let salesTax = 0;
  let total = 0;
  for (let orderLine of orderLines) {

    const lineQty = orderLine.price * orderLine.qty;

    if (orderLine.exempt === false) {
      let tax = 10 * lineQty / 100;
      let lineQtyWithTax = lineQty + tax;

      salesTax += tax;
      total += lineQtyWithTax;
    } else {
      total += lineQty;
    }

    if (orderLine.imported === true) {
      let importDuty = 5 * lineQty / 100;

      salesTax += importDuty;
      total += importDuty;
    }
  }

  salesTax = parseFloat(salesTax.toFixed(2));
  total = parseFloat(total.toFixed(2));

  return { 'salesTax' : salesTax, 'total' : total };
}

module.exports = generateReceipt;